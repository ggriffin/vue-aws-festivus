import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    grievances: [],
    // AUTH
    // user: null
  },
  mutations: {
    ADD_GRIEVANCE(state, grievance) {
      state.grievances.push(grievance);
    },
    // AUTH
    // SET_USER(state, user) {
    //   state.user = user;
    // }
  },
  actions: {
    airGrievance({
      commit
    }, grievance) {
      commit('ADD_GRIEVANCE', {
        id: this.state.grievances.length,
        grievance
      });
    },
    // AUTH
    // setUser({
    //   commit
    // }, user) {
    //   commit('SET_USER', user);
    // }
  }
})
