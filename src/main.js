import App from './App.vue'
import router from './router'
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue'
import './plugins/vuetify'

// AUTH
// import {
//   AmplifyPlugin,
//   components
// } from 'aws-amplify-vue';
// import Amplify, * as AmplifyModules from 'aws-amplify'
// import aws_exports from './aws-exports'
// Amplify.configure(aws_exports)
// Vue.use(AmplifyPlugin, AmplifyModules)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  components: {
    App,
    // AUTH
    // ...components
  },
  render: h => h(App)
}).$mount('#app')