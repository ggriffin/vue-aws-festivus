var https = require('https');
var fs = require('fs');

module.exports = {
  configureWebpack: {
    resolve: {
      // .mjs needed for https://github.com/graphql/graphql-js/issues/1272
      extensions: ['*', '.mjs', '.js', '.vue', '.json', '.gql', '.graphql']
    },
    module: {
      rules: [ 
        { // fixes https://github.com/graphql/graphql-js/issues/1272
          test: /\.mjs$/,
          include: /node_modules/,
          type: 'javascript/auto'
        },
        {
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader',
        }
      ]
    }
  },
  devServer: {
    disableHostCheck: true,
    host: 'festivus.test',
    hot: false,
    port: 8080,
    public: 'festivus.test:8080',
    //https: {
    //  key: fs.readFileSync('./festivus.test.key'),
    //  cert: fs.readFileSync('./festivus.test.cert')
    //}
  }
}
